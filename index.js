// npm init / npm int -y
// npm install express
// npm install mongoose

// We store our express module to a variable so we could easily access its keywords, functions and methods
const express = require('express');
const mongoose = require('mongoose');
const bcrypt = require("bcrypt")
const dotenv = require('dotenv');
dotenv.config();

const dbURL = process.env.DATABASE_URL;


const app = express();

const port = 3001;


// Setup for allowign the server to handle data from requests
// Allows your app to read json data

app.use(express.json());

// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}));
// Reference: https://dev.to/griffitp12/express-s-json-and-urlencoded-explained-1m7o

mongoose.connect(dbURL,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// [SECTION] Creating Schema
// Schemas determine the structure of the documents to be written in the database
// Schemas act as blueprints to our data
// Syntax:
/*
	const schemaName = new mongoose.Schema({<keyvalue:pair>});
*/
// name & status
// "required" is used to specify that a field must not be empty.
// "default" is used if a field value is not supplied.

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Task name is required"]
	},
	status: {
		type: String,
		default: "pending"
	}
});

//[SECTION] Models
// The variable/object "Task"can now used to run commands for interacting with our database
// Models must be in singular form and capitalized
// The first parameter of the Mongoose model method indicates the collection in where to store the data
// The second parameter is used to specify the Schema/blueprint of the documents that will be stored in the MongoDB collection

	// modelName			collectionName	schemaName			
const Task = mongoose.model("Task", taskSchema);


// [SECTION] POST / insert
app.post("/tasks", (req, res) => {

	console.log(req.body.name);

	// Goal: Check if there are duplicate tasks
	// "findOne" is Mongoose method that acts similar to "find" of MongoDB
	// findOne() returns the first document that matches the search criteria
	// If there are no matches, the value of result null
	// "err" is a shorthand naming convention for errors
	Task.findOne({name: req.body.name}, (err, result) => {
		if (result != null && result.name == req.body.name) {
			return res.send("Duplicate task found");

		} else {

			let newTask = new Task({
				name: req.body.name
			});

			// The "save" method will store the information to the database
			newTask.save((saveErr, savedTask) => {
				if (saveErr) {
					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error

						// Errors normally come as an object data type
					return console.error(saveErr)
				} else {

					// returns a status code of 201 - means success or OK in saving
					// sends a message "New task created" upon successful saving
					return res.status(201).send("New task created");
				}
			});
		}
	});

});

app.get("/tasks", (req, res) => {

	Task.find({}, (err, result) => {

		if (err) {
			return res.status(404).send("Error server");
		} else {

			return res.status(200).json({data: result});

		}
		
	});


})

app.get("/tasks/:name", (req, res) => {

	Task.findOne({name: req.params.name}, (err, result) => {

		if (err) {
			return res.status(404).send("Error server");
		} else {

			return res.status(200).json({data: result});

		}
		
	});


})

app.put("/tasks", (req, res) => {

	Task.update({name: req.body.name}, {name: req.body.newName}, (err, result) => {

		if (err) {
			return res.status(404).send("Error server");
		} else {

			return res.status(200).json({result: result});

		}
		
	});


})



// ACTIVITY
/*
	1. Create a User schema.
		username - string
		password - string
	2. Create a User model.
	Take a screenshot of your mongoDB collection to show that the users collection is added.
*/

const userSchema = new mongoose.Schema({
	username: {
		type: String,
		required: [true, "username is required"]
	},
	password: {
		type: String,
		required: [true, "password is required"]
	}
});


const User = mongoose.model("User", userSchema);

app.get("/users", (req, res) => {
	User.find({}, (err, result) =>{
		
		if (err) {
			return res.status(404).send("Error server");
		} else {

			return res.status(200).json({data: result});

		}

	})
})


app.get("/signin", (req, res) => {

	User.findOne({username: req.body.username}, (err, result) =>{
		console.log(result);
		if (err) {
			return res.status(404).send("Error server");
		} else {

			bcrypt.compare(req.body.password, result.password)
			.then(result => {

				if (result) {
					console.log('access granted');
					res.status(200).send('You are logged in.');

				} else {
					console.log('access denied');
					res.status(400).send('Username or password incorrect.');
				}

				
				console.log(result);

				



			})
			.catch(err => {
				console.log(err)
				return res.status(400).send("Bad request");
			})

		}

	})



})



app.post("/signup", (req, res) => {

	User.findOne({username: req.body.username}, (err, result) => {
		if (result) {
			return res.status(400).send('Username is already taken. Choose another one.')
		} else {


			bcrypt.hash(req.body.password, 10, (err, hash) => {

				saveUser(hash);
				
			});

			function saveUser(hash) {

			
			let newUser = new User({
				username: req.body.username,
				password: hash
			});
			

			// The "save" method will store the information to the database
			newUser.save((saveErr, savedUser) => {
				if (saveErr) {
					// Will print any errors found in the console
					// saveErr is an error object that will contain details about the error

						// Errors normally come as an object data type
					 console.error(saveErr)
					 return res.status(400).send('Error in saving');
				} else {

					// returns a status code of 201 - means success or OK in saving
					// sends a message "New task created" upon successful saving
					return res.status(201).send(`User with username ${savedUser.username} succesfully registered.`);
				}
			});


		}


		}
	});


});


/*
	3. Create a route for creating a user, the endpoint should be “/signup” and the http method to be used is ‘post’.
	4. Use findOne and conditional statement to check if there is already an existing username. If there is already, the program should send a response “Duplicate user found.” If there is no match  (else), it should successfully create a new user with password.
	5. If there is no error encountered during the process (include status code 201), the program should send a response “New user registered”

*/




app.listen(port, ()=> {
	console.log(`Server running at port ${port}`);
})


